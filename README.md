This is a test webapp for Electrolux job interview.

### API description

Three endpoints are available:

**/v1/device/programs** returns the list of programs available for the device (read from 'program' table).
It takes device serial number parameter (see below). Note that the programs are inserted 
to the database on the app start. 

Request example: `curl http://localhost:8080/v1/device/programs?serialNumber=15345`

**v1/device/programStarted** records the program start on the device ('device' table is updated).
It takes device serial number and program code parameters. Note that device type and
program device type must match (starting washing machine program on a refrigerator causes an error).
  
Request example: `curl -X POST 'http://localhost:8080/v1/device/programStarted?serialNumber=13333&programCode=Ref-A'`

**v1/device/programStopped** records the program stop on the device ('device' table is updated).
It takes device serial number and program code parameters. Note that the program
must be previously started by v1/device/programStarted request.

Request example: `curl -X POST 'http://localhost:8080/v1/device/programStopped?serialNumber=13333&programCode=Ref-A'`
  
### Device serial number
 
Each device has a unique serial number. The first symbol must be '1' for refrigerators or
'2' for washing machines. The rest of the symbols are ignored. 

### Starting the application 

It's necessary to create a Postgres database with the following parameters:

`db.host=localhost`

`db.port=5433`

`db.user=postgres`

`db.password=postgres`

`db.name=anagaf_electrolux`

Run './gradlew bootRun' to start the app locally. 

### Testing

It's necessary to create a Postgres database with the following parameters:

`db.host=localhost`

`db.port=5433`

`db.user=postgres`

`db.password=postgres`

`db.name=anagaf_electrolux_test`

Run './gradlew test' to start the unit tests. 