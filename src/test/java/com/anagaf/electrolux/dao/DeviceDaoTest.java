package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.Device;
import com.anagaf.electrolux.model.DeviceStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class DeviceDaoTest extends AbstractDaoTest {

    @Autowired
    DeviceDao deviceDao;

    @Test
    public void shouldFindDevice() {
        assertNull(deviceDao.findDevice("99999"));

        final String serialNumber1 = "12345";
        final Device device1 = deviceDao.findDevice(serialNumber1);
        assertEquals(serialNumber1, device1.getSerialNumber());
        assertEquals(DeviceStatus.ProgramStarted, device1.getStatus());
        assertEquals("Ref-A", device1.getProgram().getCode());

        final String serialNumber2 = "23456";
        final Device device2 = deviceDao.findDevice(serialNumber2);
        assertEquals(serialNumber2, device2.getSerialNumber());
        assertEquals(DeviceStatus.ProgramStopped, device2.getStatus());
        assertEquals("Ref-B", device2.getProgram().getCode());
    }
}
