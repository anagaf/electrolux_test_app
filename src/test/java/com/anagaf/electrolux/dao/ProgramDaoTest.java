package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class ProgramDaoTest extends AbstractDaoTest {

    @Autowired
    ProgramDao programDao;

    @Test
    public void shouldFindPrograms() {
        final List<Program> refrigeratorPrograms = programDao.findPrograms(DeviceType.Refrigerator);
        assertEquals(2, refrigeratorPrograms.size());
        assertEquals("Ref-A", refrigeratorPrograms.get(0).getCode());
        assertEquals("Ref-B", refrigeratorPrograms.get(1).getCode());

        final List<Program> washingMachinePrograms = programDao.findPrograms(DeviceType.WashingMachine);
        assertEquals(2, washingMachinePrograms.size());
        assertEquals("WM-X", washingMachinePrograms.get(0).getCode());
        assertEquals("WM-Y", washingMachinePrograms.get(1).getCode());
    }

    @Test
    public void shouldFindProgram() {
        assertNull(programDao.findProgram("XXX"));

        assertEquals(DeviceType.Refrigerator, programDao.findProgram("Ref-A").getDeviceType());
    }
}
