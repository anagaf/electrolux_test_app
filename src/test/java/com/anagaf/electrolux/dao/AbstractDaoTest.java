package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.configuration.TestDatabaseConfiguration;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = TestDatabaseConfiguration.class
)
@Transactional
public abstract class AbstractDaoTest {

    @Autowired
    DataSource dataSource;

    @Before
    public void setUp() throws Exception {
        final Resource resource = new ClassPathResource("test.sql");
        final ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(resource);
        databasePopulator.execute(dataSource);
    }
}
