package com.anagaf.electrolux.rest;

import com.anagaf.electrolux.TestUtils;
import com.anagaf.electrolux.logic.DeviceLogic;
import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DeviceController.class)
@RunWith(SpringRunner.class)
public class DeviceControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    DeviceLogic deviceLogic;

    @Test
    public void shouldReturnAvailablePrograms() throws Exception {
        final String serialNumber = "12345";
        final List<Program> programs = Arrays.asList(
                TestUtils.createProgram(DeviceType.Refrigerator, "A"),
                TestUtils.createProgram(DeviceType.Refrigerator, "B"));
        when(deviceLogic.getAvailablePrograms(serialNumber)).thenReturn(programs);

        final MvcResult result = mvc.perform(get(DeviceController.DEVICE_PATH_V_1 + DeviceController.PROGRAMS_PATH)
                .param(DeviceController.SERIAL_NUMBER_PARAM, serialNumber)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        final String resultJson = result.getResponse().getContentAsString();
        final String expectedJson = new ObjectMapper().writeValueAsString(programs);
        JSONAssert.assertEquals(expectedJson, resultJson, false);
    }

    @Test
    public void shouldHandleProgramStart() throws Exception {
        final String serialNumber = "12345";
        final String programCode = "ABC";

        mvc.perform(post(DeviceController.DEVICE_PATH_V_1 + DeviceController.PROGRAM_STARTED)
                .param(DeviceController.SERIAL_NUMBER_PARAM, serialNumber)
                .param(DeviceController.PROGRAM_CODE_PARAM, programCode)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        verify(deviceLogic).onProgramStarted(serialNumber, programCode);
    }

    @Test
    public void shouldHandleProgramStop() throws Exception {
        final String serialNumber = "12345";
        final String programCode = "ABC";

        mvc.perform(post(DeviceController.DEVICE_PATH_V_1 + DeviceController.PROGRAM_STOPPED)
                .param(DeviceController.SERIAL_NUMBER_PARAM, serialNumber)
                .param(DeviceController.PROGRAM_CODE_PARAM, programCode)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
        verify(deviceLogic).onProgramStopped(serialNumber, programCode);
    }
}
