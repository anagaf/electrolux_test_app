package com.anagaf.electrolux;

import com.anagaf.electrolux.model.Device;
import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;

public final class TestUtils {

    public static Program createProgram(DeviceType deviceType, String code) {
        final Program program = new Program();
        program.setDeviceType(deviceType);
        program.setCode(code);
        return program;
    }

    public static Device createDevice(final String serialNumber) {
        final Device device = new Device(serialNumber);
        return device;
    }
}
