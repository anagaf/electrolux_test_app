package com.anagaf.electrolux.logic;

import com.anagaf.electrolux.TestUtils;
import com.anagaf.electrolux.dao.DeviceDao;
import com.anagaf.electrolux.dao.ProgramDao;
import com.anagaf.electrolux.exceptions.DeviceNotFoundException;
import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.exceptions.UnexpectedProgramException;
import com.anagaf.electrolux.model.Device;
import com.anagaf.electrolux.model.DeviceStatus;
import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;
import com.anagaf.electrolux.utils.SerialNumberParser;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class DeviceLogicTest {
    private static final String SERIAL_NUMBER = "12345";
    private static final String PROGRAM_CODE = "XXX";
    private final DeviceType DEVICE_TYPE = DeviceType.WashingMachine;
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @MockBean
    private SerialNumberParser serialNumberParser;
    @MockBean
    private DeviceDao deviceDao;
    @MockBean
    private ProgramDao programDao;
    private DeviceLogic deviceLogic;

    @Before
    public void setUp() throws Exception {
        deviceLogic = new DeviceLogicImpl(serialNumberParser, deviceDao, programDao);
    }

    @Test
    public void shouldReturnAvailablePrograms() throws InvalidSerialNumberException {
        final DeviceType deviceType = DeviceType.Refrigerator;
        final List<Program> programs = Arrays.asList(
                TestUtils.createProgram(deviceType, "Ref-A"),
                TestUtils.createProgram(deviceType, "Ref-B")
        );
        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(deviceType);
        when(programDao.findPrograms(deviceType)).thenReturn(programs);
        assertEquals(programs, deviceLogic.getAvailablePrograms(SERIAL_NUMBER));
    }

    @Test
    public void shouldCreateDeviceOnProgramStart() throws InvalidSerialNumberException, UnexpectedProgramException {
        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DEVICE_TYPE);

        final Program program = TestUtils.createProgram(DEVICE_TYPE, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        when(deviceDao.findDevice(SERIAL_NUMBER)).thenReturn(null);

        deviceLogic.onProgramStarted(SERIAL_NUMBER, PROGRAM_CODE);

        final ArgumentCaptor<Device> device = ArgumentCaptor.forClass(Device.class);
        verify(deviceDao).makePersistent(device.capture());
        assertEquals(DEVICE_TYPE, device.getValue().getProgram().getDeviceType());
        assertEquals(PROGRAM_CODE, device.getValue().getProgram().getCode());
        assertEquals(SERIAL_NUMBER, device.getValue().getSerialNumber());
        assertEquals(DeviceStatus.ProgramStarted, device.getValue().getStatus());
    }

    @Test
    public void shouldUpdateDeviceOnProgramStart() throws InvalidSerialNumberException, UnexpectedProgramException {
        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DEVICE_TYPE);

        final Program program = TestUtils.createProgram(DEVICE_TYPE, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        final Device device = TestUtils.createDevice(SERIAL_NUMBER);
        when(deviceDao.findDevice(SERIAL_NUMBER)).thenReturn(device);

        deviceLogic.onProgramStarted(SERIAL_NUMBER, PROGRAM_CODE);

        verify(deviceDao).makePersistent(device);
        assertEquals(DEVICE_TYPE, device.getProgram().getDeviceType());
        assertEquals(PROGRAM_CODE, device.getProgram().getCode());
        assertEquals(SERIAL_NUMBER, device.getSerialNumber());
        assertEquals(DeviceStatus.ProgramStarted, device.getStatus());
    }

    @Test
    public void shouldThrowUnexpectedProgramExceptionOnProgramStart()
            throws InvalidSerialNumberException, UnexpectedProgramException {

        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(null);

        thrown.expect(UnexpectedProgramException.class);
        deviceLogic.onProgramStarted(SERIAL_NUMBER, PROGRAM_CODE);

        final Program program = TestUtils.createProgram(DeviceType.Refrigerator, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DeviceType.WashingMachine);

        thrown.expect(UnexpectedProgramException.class);
        deviceLogic.onProgramStarted(SERIAL_NUMBER, PROGRAM_CODE);
    }

    @Test
    public void shouldUpdateDeviceOnProgramStop()
            throws InvalidSerialNumberException, UnexpectedProgramException, DeviceNotFoundException {
        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DEVICE_TYPE);

        final Program program = TestUtils.createProgram(DEVICE_TYPE, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        final Device device = TestUtils.createDevice(SERIAL_NUMBER);
        when(deviceDao.findDevice(SERIAL_NUMBER)).thenReturn(device);

        deviceLogic.onProgramStopped(SERIAL_NUMBER, PROGRAM_CODE);

        verify(deviceDao).makePersistent(device);
        assertEquals(DEVICE_TYPE, device.getProgram().getDeviceType());
        assertEquals(PROGRAM_CODE, device.getProgram().getCode());
        assertEquals(SERIAL_NUMBER, device.getSerialNumber());
        assertEquals(DeviceStatus.ProgramStopped, device.getStatus());
    }

    @Test
    public void shouldThrowDeviceNotFoundExceptionOnProgramStop()
            throws InvalidSerialNumberException, UnexpectedProgramException, DeviceNotFoundException {
        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DEVICE_TYPE);

        final Program program = TestUtils.createProgram(DEVICE_TYPE, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        when(deviceDao.findDevice(SERIAL_NUMBER)).thenReturn(null);

        thrown.expect(DeviceNotFoundException.class);
        deviceLogic.onProgramStopped(SERIAL_NUMBER, PROGRAM_CODE);
    }

    @Test
    public void shouldThrowUnexpectedProgramExceptionOnProgramStop()
            throws InvalidSerialNumberException, UnexpectedProgramException, DeviceNotFoundException {

        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(null);

        thrown.expect(UnexpectedProgramException.class);
        deviceLogic.onProgramStarted(SERIAL_NUMBER, PROGRAM_CODE);

        final Program program = TestUtils.createProgram(DeviceType.Refrigerator, PROGRAM_CODE);
        when(programDao.findProgram(PROGRAM_CODE)).thenReturn(program);

        when(serialNumberParser.getDeviceType(SERIAL_NUMBER)).thenReturn(DeviceType.WashingMachine);

        thrown.expect(UnexpectedProgramException.class);
        deviceLogic.onProgramStopped(SERIAL_NUMBER, PROGRAM_CODE);
    }
}
