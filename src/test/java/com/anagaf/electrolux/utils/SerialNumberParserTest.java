package com.anagaf.electrolux.utils;

import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.model.DeviceType;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class SerialNumberParserTest {

    private final SerialNumberParser parser = new SerialNumberParserImpl();

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowInvalidSerialNumberException() throws InvalidSerialNumberException {
        thrown.expect(InvalidSerialNumberException.class);
        parser.getDeviceType(null);

        thrown.expect(InvalidSerialNumberException.class);
        parser.getDeviceType("");

        thrown.expect(InvalidSerialNumberException.class);
        parser.getDeviceType("asdcsdsd");

        thrown.expect(InvalidSerialNumberException.class);
        parser.getDeviceType("7");
    }

    @Test
    public void shouldReturnDeviceType() throws InvalidSerialNumberException {
        assertEquals(DeviceType.Refrigerator, parser.getDeviceType("12345"));
        assertEquals(DeviceType.WashingMachine, parser.getDeviceType("2adas"));
    }
}
