package com.anagaf.electrolux.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import javax.jdo.PersistenceManagerFactory;

/**
 * Abstract base DAO.
 *
 * @param <T> DAO data type
 */
@Transactional
public abstract class BaseDao<T> implements Dao<T> {

    private PersistenceManagerFactory persistenceManagerFactory;

    @Autowired
    @Qualifier("pmfProxy")
    public void setPersistenceManagerFactory(final PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManagerFactory = persistenceManagerFactory;
    }

    PersistenceManagerFactory getPersistenceManagerFactory() {
        return persistenceManagerFactory;
    }

    @Override
    public T makePersistent(final T obj) {
        return persistenceManagerFactory.getPersistenceManager().makePersistent(obj);
    }
}
