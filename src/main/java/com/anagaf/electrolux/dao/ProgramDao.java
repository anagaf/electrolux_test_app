package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;

import java.util.List;

/**
 * Program DAO.
 */
public interface ProgramDao extends Dao<Program> {

    /**
     * Finds all programs applicable for the device type.
     *
     * @param deviceType device type
     * @return programs list (may be empty)
     */
    List<Program> findPrograms(DeviceType deviceType);

    /**
     * Finds program by its code.
     *
     * @param programCode program code
     * @return program or null
     */
    Program findProgram(String programCode);
}
