package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;
import com.anagaf.electrolux.model.QProgram;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.jdo.JDOQLTypedQuery;
import javax.jdo.PersistenceManager;
import java.util.List;

/**
 * Program DAO implementation.
 */
@Transactional
@Repository
@SuppressWarnings("unused")
public class ProgramDaoImpl extends BaseDao<Program> implements ProgramDao {

    @Override
    public List<Program> findPrograms(final DeviceType deviceType) {
        final PersistenceManager pm = getPersistenceManagerFactory().getPersistenceManager();
        final JDOQLTypedQuery<Program> query = pm.newJDOQLTypedQuery(Program.class);
        final QProgram qProgram = QProgram.candidate();
        return query.filter(qProgram.deviceType.eq(deviceType)).executeList();
    }

    @Override
    public Program findProgram(final String code) {
        final PersistenceManager pm = getPersistenceManagerFactory().getPersistenceManager();
        final JDOQLTypedQuery<Program> query = pm.newJDOQLTypedQuery(Program.class);
        final QProgram qProgram = QProgram.candidate();
        return query.filter(qProgram.code.eq(code)).executeUnique();
    }
}
