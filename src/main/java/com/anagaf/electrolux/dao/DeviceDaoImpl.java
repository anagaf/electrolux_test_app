package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.Device;
import com.anagaf.electrolux.model.QDevice;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.jdo.JDOQLTypedQuery;
import javax.jdo.PersistenceManager;

/**
 * Device DAO implementation.
 */
@Transactional
@Repository
@SuppressWarnings("unused")
public class DeviceDaoImpl extends BaseDao<Device> implements DeviceDao {

    @Override
    public Device findDevice(final String serialNumber) {
        final PersistenceManager pm = getPersistenceManagerFactory().getPersistenceManager();
        final JDOQLTypedQuery<Device> query = pm.newJDOQLTypedQuery(Device.class);
        final QDevice qDevice = QDevice.candidate();
        return query.filter(qDevice.serialNumber.equalsIgnoreCase(serialNumber)).executeUnique();
    }
}
