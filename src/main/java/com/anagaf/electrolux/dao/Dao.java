package com.anagaf.electrolux.dao;

/**
 * DAO.
 *
 * @param <T> DAO data type
 */
public interface Dao<T> {

    /**
     * Makes object persistent.
     *
     * @param obj object
     * @return object
     */
    T makePersistent(T obj);
}
