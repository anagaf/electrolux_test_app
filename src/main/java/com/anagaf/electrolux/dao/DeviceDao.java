package com.anagaf.electrolux.dao;

import com.anagaf.electrolux.model.Device;

/**
 * Device DAO.
 */
public interface DeviceDao extends Dao<Device> {

    /**
     * Finds device by its serial number.
     *
     * @param serialNumber device serial number
     * @return found device or null
     */
    Device findDevice(String serialNumber);
}
