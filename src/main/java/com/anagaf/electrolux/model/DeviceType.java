package com.anagaf.electrolux.model;

/**
 * Device type.
 */
public enum DeviceType {
    Refrigerator(1),
    WashingMachine(2);

    final int code;

    DeviceType(final int code) {
        this.code = code;
    }

    public static DeviceType fromCode(final int code) {
        for (DeviceType deviceType : DeviceType.values()) {
            if (deviceType.code == code) {
                return deviceType;
            }
        }
        throw new IllegalArgumentException();
    }
}
