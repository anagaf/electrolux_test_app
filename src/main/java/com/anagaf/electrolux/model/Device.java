package com.anagaf.electrolux.model;

import javax.jdo.annotations.*;

/**
 * Device.
 */
@PersistenceCapable(identityType = IdentityType.APPLICATION, table = "device", detachable = "true")
public class Device {

    @Persistent(primaryKey = "true", valueStrategy = IdGeneratorStrategy.INCREMENT)
    private long id;

    @Column(name = "serial_number", length = 32)
    private String serialNumber;

    @Column(name = "program_id")
    private Program program;

    @Column(name = "status", length = 32)
    private DeviceStatus status;

    public Device(final String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setProgram(final Program program) {
        this.program = program;
    }

    public Program getProgram() {
        return program;
    }

    public void setStatus(final DeviceStatus status) {
        this.status = status;
    }

    public DeviceStatus getStatus() {
        return status;
    }
}
