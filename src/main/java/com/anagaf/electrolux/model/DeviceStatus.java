package com.anagaf.electrolux.model;

/**
 * Device status.
 */
public enum DeviceStatus {
    ProgramStarted,
    ProgramStopped
}
