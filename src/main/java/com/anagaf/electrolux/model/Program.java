package com.anagaf.electrolux.model;

import javax.jdo.annotations.*;

/**
 * Program.
 */
@PersistenceCapable(identityType = IdentityType.APPLICATION, table = "program", detachable = "true")
public class Program {

    @Persistent(primaryKey = "true", valueStrategy = IdGeneratorStrategy.INCREMENT)
    private long id;

    @Column(length = 32, name = "code")
    private String code;

    @Column(length = 32, name = "device_type")
    private DeviceType deviceType;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(final DeviceType deviceType) {
        this.deviceType = deviceType;
    }
}
