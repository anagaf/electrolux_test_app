package com.anagaf.electrolux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Application.
 */
@SpringBootApplication
@PropertySource(value = {"classpath:application.properties"})
public class App {

    public static void main(final String[] args) {
        SpringApplication.run(App.class, args);
    }

}
