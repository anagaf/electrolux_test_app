package com.anagaf.electrolux.utils;

import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.model.DeviceType;

/**
 * Serial number parser.
 */
public interface SerialNumberParser {

    /**
     * Extracts device type from serial number.
     *
     * @param serialNumber serial number
     * @return device type
     * @throws InvalidSerialNumberException .
     */
    DeviceType getDeviceType(String serialNumber) throws InvalidSerialNumberException;
}
