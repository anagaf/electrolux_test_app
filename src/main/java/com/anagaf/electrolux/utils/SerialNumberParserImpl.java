package com.anagaf.electrolux.utils;

import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.model.DeviceType;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Serial number parser implementation.
 */
@Component
public class SerialNumberParserImpl implements SerialNumberParser {

    @Override
    public DeviceType getDeviceType(final String serialNumber) throws InvalidSerialNumberException {
        if (StringUtils.isEmpty(serialNumber)) {
            throw new InvalidSerialNumberException();
        }
        final int deviceTypeCode = Character.getNumericValue(serialNumber.charAt(0));
        try {
            return DeviceType.fromCode(deviceTypeCode);
        } catch (IllegalArgumentException ex) {
            throw new InvalidSerialNumberException();
        }
    }
}
