package com.anagaf.electrolux.logic;

import com.anagaf.electrolux.exceptions.DeviceNotFoundException;
import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.exceptions.UnexpectedProgramException;
import com.anagaf.electrolux.model.Program;

import java.util.List;

/**
 * Device logic.
 */
public interface DeviceLogic {

    /**
     * Returns all programs available for the device.
     *
     * @param serialNumber device serial number
     * @return program list (may be empty)
     * @throws InvalidSerialNumberException .
     */
    List<Program> getAvailablePrograms(String serialNumber) throws InvalidSerialNumberException;

    /**
     * Handles program start.
     *
     * @param serialNumber device serial number
     * @param programCode  program code
     * @throws InvalidSerialNumberException .
     * @throws UnexpectedProgramException   .
     */
    void onProgramStarted(String serialNumber, String programCode)
            throws InvalidSerialNumberException, UnexpectedProgramException;

    /**
     * Handles program stop.
     *
     * @param serialNumber device serial number
     * @param programCode  program code
     * @throws InvalidSerialNumberException .
     * @throws UnexpectedProgramException   .
     * @throws DeviceNotFoundException      .
     */
    void onProgramStopped(String serialNumber, String programCode)
            throws UnexpectedProgramException, InvalidSerialNumberException, DeviceNotFoundException;
}
