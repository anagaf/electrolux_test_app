package com.anagaf.electrolux.logic;

import com.anagaf.electrolux.dao.DeviceDao;
import com.anagaf.electrolux.dao.ProgramDao;
import com.anagaf.electrolux.exceptions.DeviceNotFoundException;
import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.exceptions.UnexpectedProgramException;
import com.anagaf.electrolux.model.Device;
import com.anagaf.electrolux.model.DeviceStatus;
import com.anagaf.electrolux.model.DeviceType;
import com.anagaf.electrolux.model.Program;
import com.anagaf.electrolux.utils.SerialNumberParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
@Transactional
@SuppressWarnings("unused")
public class DeviceLogicImpl implements DeviceLogic {

    private final SerialNumberParser serialNumberParser;

    private final DeviceDao deviceDao;

    private final ProgramDao programDao;

    @Autowired
    public DeviceLogicImpl(final SerialNumberParser serialNumberParser,
                           final DeviceDao deviceDao,
                           final ProgramDao programDao) {
        this.serialNumberParser = serialNumberParser;
        this.deviceDao = deviceDao;
        this.programDao = programDao;
    }

    @Override
    public List<Program> getAvailablePrograms(final String serialNumber) throws InvalidSerialNumberException {
        final DeviceType deviceType = serialNumberParser.getDeviceType(serialNumber);
        return programDao.findPrograms(deviceType);
    }

    @Override
    public void onProgramStarted(final String serialNumber, final String programCode)
            throws InvalidSerialNumberException, UnexpectedProgramException {
        final Program program = getProgram(serialNumber, programCode);
        Device device = deviceDao.findDevice(serialNumber);
        if (device == null) {
            device = new Device(serialNumber);
        }
        device.setProgram(program);
        device.setStatus(DeviceStatus.ProgramStarted);
        deviceDao.makePersistent(device);
    }

    @Override
    public void onProgramStopped(final String serialNumber, final String programCode)
            throws UnexpectedProgramException, InvalidSerialNumberException, DeviceNotFoundException {
        final Program program = getProgram(serialNumber, programCode);
        final Device device = deviceDao.findDevice(serialNumber);
        if (device == null) {
            throw new DeviceNotFoundException();
        }
        device.setProgram(program);
        device.setStatus(DeviceStatus.ProgramStopped);
        deviceDao.makePersistent(device);
    }

    private Program getProgram(final String serialNumber, final String programCode)
            throws UnexpectedProgramException, InvalidSerialNumberException {
        final Program program = programDao.findProgram(programCode);
        if (program == null) {
            throw new UnexpectedProgramException("Program " + programCode + " not found");
        }
        final DeviceType deviceType = serialNumberParser.getDeviceType(serialNumber);
        if (program.getDeviceType() != deviceType) {
            throw new UnexpectedProgramException("Program " + programCode + " is not applicable to device type " + deviceType);
        }
        return program;
    }
}
