package com.anagaf.electrolux.configuration;

import com.zaxxer.hikari.HikariDataSource;
import liquibase.integration.spring.SpringLiquibase;
import org.datanucleus.api.jdo.JDOPersistenceManagerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.jdo.JdoTransactionManager;
import org.springframework.orm.jdo.TransactionAwarePersistenceManagerFactoryProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration for the underlying database and related stuff (liquibase, transaction manager etc.)
 */
@Configuration
@EnableTransactionManagement
public class DatabaseConfiguration {
    /**
     * Environment injection.
     */
    @Autowired
    Environment env;
    /**
     * Connection driver name.
     */
    @Value("${datanucleus.ConnectionDriverName}")
    private String connectionDriverName;
    /**
     * Connection host name.
     */
    @Value("${db.host}")
    private String hostName;
    /**
     * Connection port.
     */
    @Value("${db.port}")
    private int port;
    /**
     * Connection database name.
     */
    @Value("${db.name}")
    private String dbName;
    /**
     * Connection user name.
     */
    @Value("${db.user}")
    private String userName;
    /**
     * Connection user password.
     */
    @Value("${db.password}")
    private String password;
    /**
     * This property controls the maximum size that the pool is allowed to reach,
     * including both idle and in-use connections.
     * Basically this value will determine the maximum number of actual connections to the database backend.
     * A reasonable value for this is best determined by your execution environment.
     */
    @Value("${connection.pool.maximumPoolSize}")
    private int maximumPoolSize;
    /**
     * /**
     * Maximum amount of time that a connection is allowed to sit idle in the pool.
     */
    @Value("${connection.pool.idleTimeout}")
    private long idleTimeout;
    /**
     * This property controls the maximum lifetime of a connection in the pool.
     * An in-use connection will never be retired, only when it is closed will it then be removed.
     */
    @Value("${connection.pool.maxLifetime}")
    private long maxLifetime;

    /**
     * Datasource with Hikari connection pool support.
     *
     * @return {@link HikariDataSource}
     */
    @Bean(destroyMethod = "close")
    public HikariDataSource dataSource() {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(connectionDriverName);
        hikariDataSource.setJdbcUrl("jdbc:postgresql://" + hostName + ":" + port + "/" + dbName);
        hikariDataSource.setUsername(userName);
        hikariDataSource.setPassword(password);
        hikariDataSource.setMaximumPoolSize(maximumPoolSize);
        hikariDataSource.setIdleTimeout(idleTimeout);
        hikariDataSource.setMaxLifetime(maxLifetime);
        return hikariDataSource;
    }

    /**
     * Liquibase support.
     *
     * @param dataSource {@link DataSource}
     * @return {@link SpringLiquibase}
     */
    @Bean()
    public SpringLiquibase liquibase(final DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:/database/changelog.xml");
        Map params = new HashMap();
        params.put("db.name", dbName);
        liquibase.setChangeLogParameters(params);
        return liquibase;
    }

    /**
     * JDO persistance manager factory setup.
     *
     * @param dataSource {@link DataSource}
     * @return {@link JDOPersistenceManagerFactory}
     */
    @Bean(destroyMethod = "close")
    public JDOPersistenceManagerFactory pmf(final DataSource dataSource) {
        // Read all supported properties
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocations(
                new ClassPathResource("classpath:application.properties"),
                new ClassPathResource("classpath:local.properties"));
        JDOPersistenceManagerFactory pmf = new JDOPersistenceManagerFactory(readProperties());
        pmf.setConnectionFactory(dataSource);
        pmf.setNontransactionalRead(true);
        return pmf;
    }

    /**
     * JDO transaction manager setup.
     *
     * @param pmf {@link JDOPersistenceManagerFactory}
     * @return {@link JdoTransactionManager}
     */
    @Bean
    public JdoTransactionManager txManager(final JDOPersistenceManagerFactory pmf) {
        JdoTransactionManager txManager = new JdoTransactionManager();
        txManager.setPersistenceManagerFactory(pmf);
        return txManager;
    }

    /**
     * {@link TransactionAwarePersistenceManagerFactoryProxy} setup.
     *
     * @param pmf {@link JDOPersistenceManagerFactory}
     * @return {@link TransactionAwarePersistenceManagerFactoryProxy}
     */
    @Bean
    public TransactionAwarePersistenceManagerFactoryProxy pmfProxy(final JDOPersistenceManagerFactory pmf) {
        TransactionAwarePersistenceManagerFactoryProxy pmfProxy = new TransactionAwarePersistenceManagerFactoryProxy();
        pmfProxy.setTargetPersistenceManagerFactory(pmf);
        pmfProxy.setAllowCreate(false);
        return pmfProxy;
    }

    /**
     * Allows to read provided configuration files properties as map and inject into builders
     * (e.g. {@link JDOPersistenceManagerFactory})
     *
     * @return map of properties
     */
    private Map<String, Object> readProperties() {
        final Map<String, Object> map = new HashMap();
        for (org.springframework.core.env.PropertySource propertySource :
                ((AbstractEnvironment) env).getPropertySources()) {
            if (propertySource instanceof MapPropertySource) {
                map.putAll(((MapPropertySource) propertySource).getSource());
            }
        }
        return map;
    }
}