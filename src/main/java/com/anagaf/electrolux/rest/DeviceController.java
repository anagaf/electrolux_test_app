package com.anagaf.electrolux.rest;

import com.anagaf.electrolux.exceptions.DeviceNotFoundException;
import com.anagaf.electrolux.exceptions.InvalidSerialNumberException;
import com.anagaf.electrolux.exceptions.UnexpectedProgramException;
import com.anagaf.electrolux.logic.DeviceLogic;
import com.anagaf.electrolux.model.Program;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Device REST controller.
 */
@RestController
@Transactional
@SuppressWarnings("unused")
public class DeviceController {

    private static final String V1 = "/v1";
    private static final String DEVICE_PATH = "/device";
    static final String DEVICE_PATH_V_1 = V1 + DEVICE_PATH;
    static final String PROGRAMS_PATH = "/programs";
    static final String PROGRAM_STARTED = "/programStarted";
    static final String PROGRAM_STOPPED = "/programStopped";

    static final String SERIAL_NUMBER_PARAM = "serialNumber";
    static final String PROGRAM_CODE_PARAM = "programCode";

    private final DeviceLogic deviceLogic;

    @Autowired
    public DeviceController(DeviceLogic deviceLogic) {
        this.deviceLogic = deviceLogic;
    }

    /**
     * Returns all programs available for the device.
     *
     * @param serialNumber device serial number
     * @return program list (may be empty)
     * @throws InvalidSerialNumberException .
     */
    @RequestMapping(value = DEVICE_PATH_V_1 + PROGRAMS_PATH, method = RequestMethod.GET)
    public List<Program> getPrograms(
            @RequestParam(SERIAL_NUMBER_PARAM) final String serialNumber) throws InvalidSerialNumberException {
        return deviceLogic.getAvailablePrograms(serialNumber);
    }

    /**
     * Handles program start.
     *
     * @param serialNumber device serial number
     * @param programCode  program code
     * @throws InvalidSerialNumberException .
     * @throws UnexpectedProgramException   .
     */
    @RequestMapping(value = DEVICE_PATH_V_1 + PROGRAM_STARTED, method = RequestMethod.POST)
    public void onProgramStarted(
            @RequestParam(SERIAL_NUMBER_PARAM) final String serialNumber,
            @RequestParam(PROGRAM_CODE_PARAM) final String programCode)
            throws InvalidSerialNumberException, UnexpectedProgramException {
        deviceLogic.onProgramStarted(serialNumber, programCode);
    }

    /**
     * Handles program stop.
     *
     * @param serialNumber device serial number
     * @param programCode  program code
     * @throws InvalidSerialNumberException .
     * @throws UnexpectedProgramException   .
     * @throws DeviceNotFoundException      .
     */
    @RequestMapping(value = DEVICE_PATH_V_1 + PROGRAM_STOPPED, method = RequestMethod.POST)
    public void onProgramStopped(
            @RequestParam(SERIAL_NUMBER_PARAM) final String serialNumber,
            @RequestParam(PROGRAM_CODE_PARAM) final String programCode)
            throws InvalidSerialNumberException, UnexpectedProgramException, DeviceNotFoundException {
        deviceLogic.onProgramStopped(serialNumber, programCode);
    }
}
