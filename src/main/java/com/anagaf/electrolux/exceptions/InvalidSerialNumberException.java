package com.anagaf.electrolux.exceptions;

/**
 * Exception thrown if serial number is invalid.
 */
public class InvalidSerialNumberException extends Exception {
}
