package com.anagaf.electrolux.exceptions;

/**
 * Exception thrown if device is not found.
 */
public class DeviceNotFoundException extends Exception {
}
