package com.anagaf.electrolux.exceptions;

/**
 * Exception thrown if program is unexpected.
 */
public class UnexpectedProgramException extends Exception {
    public UnexpectedProgramException(final String msg) {
        super(msg);
    }
}
